#!/bin/bash

for file in ./*.tex
do
    if [ -f "${file}" ]; then
      pdflatex $file;
      rm $(ls -I "*.tex" -I "*.sh" -I "*.pdf")
    break
    fi
done
