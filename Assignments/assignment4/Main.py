#!/usr/bin/python
import os
import glob
import re
import linecache

#Reads all of the ".txt" files within the directory
file_list = glob.glob(os.path.join(os.getcwd(), "/home/geary/cosc362geary/Assignments/assignment4a", "*.txt"))

################################################################################################################
#Creates two lists (Corpus = all of the data inside each of the files
#                  (Names  = all of the names in each file))
corpus = []
names  = []
################################################################################################################
#Loops over the files and stores the data into each of the two lists
for file in file_list:
    currentFile = open(file)
    name = currentFile.readlines()
    names.append(name[0])

print "Reading in the names: " + (str(names))

for file_path in file_list:
    with open(file_path) as f_input:
        corpus.append(f_input.read())
o = open("TotalList.log", "w")
################################################################################################################
#Loops over the corpus data and grabs the numbers based off of certain characters
for n in range(0,len(corpus[:])):
 hours = 0
 for char in range(0,len(corpus[n])):
    if corpus[n][char] == ('h'):
      if corpus[n][char + 1] == ('r'):
        hours += int(corpus[n][char - 1])

 minutes = 0
 for char in range(0,len(corpus[n])):
    if corpus[n][char] == ('m'):
      if corpus[n][char + 1] == ('i'):
        if corpus[n][char + 2] == ('n'):
         minutes += int(corpus[n][char - 1])
         minutes += int(corpus[n][char - 2])* 10
#######################################################
#Calculates the minutes and hours and inserts them into a file
 min = minutes / 60
 minm = minutes % 60
 TotalH = min + hours
 Total = str(TotalH) + "hr " + str(minm) + "min"
 o.write(str(names[n]))
 o.write(Total + "\n")
#######################################################

